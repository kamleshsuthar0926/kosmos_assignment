package com.harisewak.kosmos_assignment.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.insertSeparators
import androidx.paging.map
import com.harisewak.kosmos_assignment.repository.Repository
import com.harisewak.kosmos_assignment.repository.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

//    private var currentSearchResult: Flow<PagingData<UiModel>>? = null

    fun getUsers(): Flow<PagingData<UiModel>> {
//        val lastResult = currentSearchResult
        return repository.getUsers()
            .map { pagingData -> pagingData.map { UiModel.UserItem(it) } }
            .map {
                it.insertSeparators<UiModel.UserItem, UiModel> { before, after ->
                    if (after == null) {
                        // we're at the end of the list
                        return@insertSeparators null
                    }

                    if (before == null) {
                        // we're at the beginning of the list
                        return@insertSeparators UiModel.SeparatorItem("Beginning..")
                    }
                    // check between 2 items
                    UiModel.SeparatorItem("In between..")
                }
            }
            .cachedIn(viewModelScope)
    }
}

sealed class UiModel {
    data class UserItem(val user: User) : UiModel()
    data class SeparatorItem(val description: String) : UiModel()
}
