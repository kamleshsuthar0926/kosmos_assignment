package com.harisewak.kosmos_assignment.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.harisewak.kosmos_assignment.R
import com.harisewak.kosmos_assignment.repository.User

class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val firstName: TextView = view.findViewById(R.id.tv_first_name)
    private val lastName: TextView = view.findViewById(R.id.tv_last_name)
    private val email: TextView = view.findViewById(R.id.tv_email)
    private val picture: ImageView = view.findViewById(R.id.iv_photo)
    private var user: User? = null

    fun bind(user: User?) {
        if (user == null) {
            val resources = itemView.resources
            firstName.text = resources.getString(R.string.loading)
            lastName.visibility = View.GONE
            email.visibility = View.GONE
        } else {
            showUserData(user)
            itemView.setOnClickListener {
                val action =
                    ListFragmentDirections.actionListFragmentToUpdateFragment(user)
                itemView.findNavController().navigate(action)
            }
        }
    }

    private fun showUserData(user: User) {
        this.user = user
        firstName.text = user.first_name
        lastName.text = user.last_name
        email.text = user.email
        picture.load(user.avatar) {
            crossfade(true)
            if (user.avatar.isEmpty()) {
                error(R.drawable.ic_placeholder)
            } else {
                error(R.drawable.ic_error)
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup): UserViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_user, parent, false)
            return UserViewHolder(view)
        }
    }
}
