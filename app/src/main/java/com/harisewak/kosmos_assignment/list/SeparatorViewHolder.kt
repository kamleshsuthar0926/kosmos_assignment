package com.harisewak.kosmos_assignment.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.harisewak.kosmos_assignment.R

class SeparatorViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        fun create(parent: ViewGroup): SeparatorViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_separator, parent, false)
            return SeparatorViewHolder(view)
        }
    }
}
