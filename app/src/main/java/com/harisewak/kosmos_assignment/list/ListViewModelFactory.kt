package com.harisewak.kosmos_assignment.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.harisewak.kosmos_assignment.repository.Repository
import com.harisewak.kosmos_assignment.update.UpdateViewModel
import javax.inject.Inject

class ListViewModelFactory constructor(private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
//        if (modelClass.isAssignableFrom(ListViewModel::class.java)) {
//            @Suppress("UNCHECKED_CAST")
//            return ListViewModel(repository) as T
//        }
//        if (modelClass.isAssignableFrom(UpdateViewModel::class.java)) {
//            @Suppress("UNCHECKED_CAST")
//            return UpdateViewModel(repository) as T
//        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}