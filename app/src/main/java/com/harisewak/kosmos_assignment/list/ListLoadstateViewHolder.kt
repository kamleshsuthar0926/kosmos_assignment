package com.harisewak.kosmos_assignment.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.harisewak.kosmos_assignment.R

class ListLoadStateViewHolder(
    private val itemView: View,
    retry: () -> Unit
) : RecyclerView.ViewHolder(itemView) {

    init {
        itemView.findViewById<View>(R.id.retry_button).setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        if (loadState is LoadState.Error) {
            itemView.findViewById<TextView>(R.id.error_msg).text = loadState.error.localizedMessage
        }
        itemView.findViewById<View>(R.id.progress_bar).isVisible = loadState is LoadState.Loading
        itemView.findViewById<View>(R.id.retry_button).isVisible = loadState !is LoadState.Loading
        itemView.findViewById<View>(R.id.error_msg).isVisible = loadState !is LoadState.Loading
    }

    companion object {
        fun create(parent: ViewGroup, retry: () -> Unit): ListLoadStateViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_load_state, parent, false)
            return ListLoadStateViewHolder(view, retry)
        }
    }
}
