package com.harisewak.kosmos_assignment.update

import android.net.Uri
import androidx.lifecycle.ViewModel
import com.harisewak.kosmos_assignment.repository.Repository
import com.harisewak.kosmos_assignment.repository.User
import dagger.hilt.android.lifecycle.HiltViewModel
import java.io.File
import javax.inject.Inject

@HiltViewModel
class UpdateViewModel @Inject constructor(val repository: Repository): ViewModel() {
    // Transferring File and Uri members from UpdateFragment to avoid NPE in landscape mode
    var file: File? = null
    var uri: Uri? = null

    suspend fun insert(user: User): User {
        return repository.insert(user)
    }

    suspend fun update(user: User) {
        repository.update(user)
    }
}