package com.harisewak.kosmos_assignment.update

import android.annotation.TargetApi
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.harisewak.kosmos_assignment.R
import com.harisewak.kosmos_assignment.databinding.FragmentUpdateBinding
import com.harisewak.kosmos_assignment.repository.User
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.io.*


private const val RQ_SELECT_IMAGE = 11;
private const val TAG = "UpdateFragment"
private const val COLON_SEPARATOR = ":"
private const val IMAGE = "image"

@AndroidEntryPoint
class UpdateFragment : Fragment(), ImageChooser {
    private lateinit var binding: FragmentUpdateBinding
    val viewModel: UpdateViewModel by viewModels()
    val args: UpdateFragmentArgs by navArgs()
    private var dialog: AlertDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUpdateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.btSubmit.setOnClickListener { onSubmitClicked() }
        binding.ivPhoto.setOnClickListener { selectPhoto() }
        setUserData()
    }

    private fun selectPhoto() {
        if (dialog == null) {
            dialog = AlertDialog.Builder(requireContext())
                .setItems(
                    arrayOf("Click picture", "Select picture")
                ) { dialog: DialogInterface, position: Int ->
                    if (position == 0) {
                        onCameraClicked()
                    } else {
                        onGalleryClicked()
                    }
                    dialog.dismiss()
                }
                .create()
        }
            dialog!!.show()
    }

    override fun onCameraClicked() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        var imageName = "temp.jpg"
        args.userData?.let {
            imageName = "${it.id}.jpg"
        }
        viewModel.file = File(
            context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
                .absolutePath + File.separator.toString() + imageName
        )
        viewModel.uri = FileProvider.getUriForFile(
            requireContext(),
            requireContext().packageName + ".fileprovider",
            viewModel.file!!
        )
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, viewModel.uri)
        startActivityForResult(cameraIntent, RQ_SELECT_IMAGE)
    }

    override fun onGalleryClicked() {
        val galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.setType("image/*")
        startActivityForResult(galleryIntent, RQ_SELECT_IMAGE)
        var imageName = "temp.jpg"
        args.userData?.let {
            imageName = "${it.id}.jpg"
        }
        viewModel.file = File(
            context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
                .absolutePath + File.separator.toString() + imageName
        )
    }

    private fun setUserData() {
        args.userData?.let {
            with(binding) {
                etFirstName.setText(it.first_name)
                etLastName.setText(it.last_name)
                etEmail.setText(it.email)
                ivPhoto.load(it.avatar) {
                    crossfade(true)
                    placeholder(R.drawable.ic_placeholder)
                    if (it.avatar.isEmpty()) {
                        error(R.drawable.ic_placeholder)
                    } else {
                        error(R.drawable.ic_error)
                    }
                }
            }
        }
    }

    private fun onSubmitClicked() {
        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val email = binding.etEmail.text.toString()

        if (firstName.isBlank() || lastName.isBlank() || email.isBlank()) {
            Toast.makeText(
                context,
                "Please enter all fields to proceed",
                Toast.LENGTH_LONG
            ).show()
        } else if (!email.matches(Patterns.EMAIL_ADDRESS.toRegex())) {
            Toast.makeText(
                context,
                "Please enter a valid email",
                Toast.LENGTH_LONG
            ).show()
        } else {
            lifecycleScope.launch {
                when (args.userData) {
                    null -> {
                        val user = viewModel.insert(User(email, firstName, lastName))
                        viewModel.file?.let {
                            val newFile = File(
                                context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
                                    .absolutePath + File.separator.toString() + "{$user.id}.jpg"
                            )
                            val renamingSuccessful = it.renameTo(newFile)
                            if (renamingSuccessful) {
                                user.avatar = newFile.toUri().toString()
                                viewModel.update(user)
                                it.delete()
                            }
                        }
                    }
                    else -> {
                        val user = User(email, firstName, lastName, args.userData!!.avatar)
                        user.id = args.userData!!.id
                        viewModel.file?.let {
                            user.avatar = it.toUri().toString()
                        }
                        viewModel.update(user)
                    }
                }
                findNavController().popBackStack()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_SELECT_IMAGE && resultCode == Activity.RESULT_OK) {
            if (viewModel.uri == null) {
                viewModel.uri = data?.data
                compressImage(viewModel.uri!!)
            }
            binding.ivPhoto.load(viewModel.uri) {
                crossfade(true)
                placeholder(R.drawable.ic_placeholder)
                error(R.drawable.ic_error)
            }
        }
    }

    private fun compressImage(selectedImage: Uri): Bitmap? {
        val image = bitmapWithCorrectOrientation(selectedImage)

        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos) //Compression quality, here 100 means no compression, the storage of compressed data to baos

        var options = 90
        while (baos.toByteArray().size / 1024 > 1000) { //Loop if compressed picture is greater than 1 MB, than to compression
            baos.reset() //Reset baos is empty baos
            image.compress(Bitmap.CompressFormat.JPEG, options, baos) //The compression options%, storing the compressed data to the baos
            options -= 10 //Every time reduced by 10
        }

        // If compressed image is larger than 1 MB, show error
        if (baos.toByteArray().size / 1024 > 1024) {
            Toast.makeText(requireContext(), "Image size too high, kindly select a smaller image", Toast.LENGTH_LONG).show()
            return null;
        }

        val fos = FileOutputStream(viewModel.file)
        fos.write(baos.toByteArray())
        fos.close()

        if (image == null) {
            Toast.makeText(requireContext(), "Image compression failed", Toast.LENGTH_LONG).show()
            return null;
        }

        return image
    }

    @Throws(FileNotFoundException::class)
    fun bitmapWithCorrectOrientation(bitmapUri: Uri): Bitmap {
        val `is`: InputStream = requireContext().getContentResolver().openInputStream(bitmapUri)!!
        var bitmap = BitmapFactory.decodeStream(`is`, null, null)
        val imgRotation: Int = getImageRotationDegrees(bitmapUri)
        var endRotation = if (imgRotation < 0) -imgRotation else imgRotation
        endRotation %= 360
        endRotation = 90 * (endRotation / 90)
        if (endRotation > 0 && bitmap != null) {
            val m = Matrix()
            m.setRotate(endRotation.toFloat())
            val tmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, m, true)
            if (tmp != null) {
                bitmap.recycle()
                bitmap = tmp
            }
        }
        return bitmap!!
    }

    fun getImageRotationDegrees(imgUri: Uri): Int {
        var photoRotation = ExifInterface.ORIENTATION_UNDEFINED
        try {
            var hasRotation = false
            //If image comes from the gallery and is not in the folder DCIM (Scheme: content://)
            val projection = arrayOf(MediaStore.Images.ImageColumns.ORIENTATION)
            val cursor: Cursor = requireContext().getContentResolver().query(imgUri, projection, null, null, null)!!
            if (cursor != null) {
                if (cursor.columnCount > 0 && cursor.moveToFirst()) {
                    photoRotation = cursor.getInt(cursor.getColumnIndex(projection[0]))
                    hasRotation = photoRotation != 0
                    Log.d("Cursor orientation: ", photoRotation.toString())
                }
                cursor.close()
            }
            //If image comes from the camera (Scheme: file://) or is from the folder DCIM (Scheme: content://)
            if (!hasRotation) {
                val exif = ExifInterface(getAbsolutePath(imgUri)!!)
                val exifRotation: Int = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL)
                when (exifRotation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> {
                        photoRotation = 90
                    }
                    ExifInterface.ORIENTATION_ROTATE_180 -> {
                        photoRotation = 180
                    }
                    ExifInterface.ORIENTATION_ROTATE_270 -> {
                        photoRotation = 270
                    }
                }
                Log.d(TAG, "Exif orientation: $photoRotation")
            }
        } catch (e: IOException) {
            Log.e(TAG, "Error determining rotation for image$imgUri", e)
        }
        return photoRotation
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private fun getAbsolutePath(uri: Uri): String? { //Code snippet edited from: http://stackoverflow.com/a/20559418/2235133
        var filePath = uri.path
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) { // Will return "image:x*"
            val wholeID = TextUtils.split(DocumentsContract.getDocumentId(uri), COLON_SEPARATOR)
            // Split at colon, use second item in the array
            val type = wholeID[0]
            if (IMAGE.equals(type, ignoreCase = true)) { //If it not type image, it means it comes from a remote location, like Google Photos
                val id = wholeID[1]
                val column = arrayOf(MediaStore.Images.Media.DATA)
                // where id is equal to
                val sel = MediaStore.Images.Media._ID + "=?"
                val cursor: Cursor = requireContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, arrayOf(id), null)!!
                if (cursor != null) {
                    val columnIndex = cursor.getColumnIndex(column[0])
                    if (cursor.moveToFirst()) {
                        filePath = cursor.getString(columnIndex)
                    }
                    cursor.close()
                }
                Log.d(TAG, "Fetched absolute path for uri$uri")
            }
        }
        return filePath
    }

//    class ImageChooserDialog() : BottomSheetDialogFragment() {
//        init {
//            val chooser: ImageChooser
//        }
//        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
//            AlertDialog.Builder(requireContext())
//                .setItems(arrayOf("Click picture", "Select picture")
//                ) { dialog: DialogInterface, position: Int ->
//                    if (position == 0) {
//                        chooser.onCameraClicked()
//                    } else {
//                        chooser.onGalleryClicked()
//                    }
//                    dialog.dismiss()
//                }
//                .create()
//
//        companion object {
//            const val TAG = "ImageChooserDialog"
//        }
//    }

}

public interface ImageChooser {
    fun onCameraClicked()
    fun onGalleryClicked()
}
