package com.harisewak.kosmos_assignment.update

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.harisewak.kosmos_assignment.list.ListViewModel
import com.harisewak.kosmos_assignment.repository.Repository
import javax.inject.Inject

class UpdateViewModelFactory constructor(private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
//        if (modelClass.isAssignableFrom(UpdateViewModel::class.java)) {
//            @Suppress("UNCHECKED_CAST")
//            return UpdateViewModel(repository) as T
//        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}