package com.harisewak.kosmos_assignment.repository

import androidx.paging.PagingSource
import androidx.room.*

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(repos: List<User>)

    @Query("SELECT * FROM user")
    fun getUsers(): PagingSource<Int, User>

    @Query("DELETE FROM user")
    suspend fun clear()

    @Insert
    suspend fun insert(user: User)

    @Query("SELECT COUNT (*) FROM user")
    suspend fun getUserCount(): Int

    @Update
    suspend fun update(user: User)

    @Query("SELECT * FROM user ORDER BY id DESC LIMIT 1")
    suspend fun getLastInsertedUser(): User

}