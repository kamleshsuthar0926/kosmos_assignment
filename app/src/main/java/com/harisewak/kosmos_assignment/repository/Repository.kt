package com.harisewak.kosmos_assignment.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository class that works with local and remote data sources.
 */
@Singleton
class Repository @Inject constructor(
    private val service: Api,
    private val database: UserDatabase
) {

    @OptIn(ExperimentalPagingApi::class)
    fun getUsers(): Flow<PagingData<User>> {

        val pagingSourceFactory = { database.userDao().getUsers() }

        return Pager(
            config = PagingConfig(pageSize = NETWORK_PAGE_SIZE, enablePlaceholders = false),
            remoteMediator = UserRemoteMediator(
                service,
                database
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    suspend fun insert(user: User): User {
        database.userDao().insert(user)
        return database.userDao().getLastInsertedUser()
    }

    suspend fun update(user: User) {
        database.userDao().update(user)
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 10
    }
}