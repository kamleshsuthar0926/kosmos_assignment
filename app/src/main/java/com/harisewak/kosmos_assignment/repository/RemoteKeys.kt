package com.harisewak.kosmos_assignment.repository

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "remote_keys")
data class RemoteKeys(
    @PrimaryKey val user_id: Int,
    val prev_key: Int?,
    val next_key: Int?
)