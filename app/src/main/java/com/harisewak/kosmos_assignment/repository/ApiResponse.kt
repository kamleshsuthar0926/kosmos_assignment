package com.harisewak.kosmos_assignment.repository

data class ApiResponse(
    val page: Int,
    val per_page: Int,
    val total: Int,
    val total_pages: Int,
    val data: List<User> = emptyList())
